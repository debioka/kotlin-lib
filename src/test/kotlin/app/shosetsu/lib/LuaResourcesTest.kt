package app.shosetsu.lib

import app.shosetsu.lib.lua.coerceLuaToJava
import app.shosetsu.lib.lua.shosetsuGlobals
import org.junit.Test

/**
 * 15 / 01 / 2023
 */
class LuaResourcesTest {
	private val globals = shosetsuGlobals()

	@Test
	fun chapterTypeTest() {
		Novel.ChapterType.values().forEach { type ->
			val result = globals.load("return ChapterType.${type.name}", "chapterTypeTest").call()
			assert(coerceLuaToJava<Novel.ChapterType>(result) == type)
		}
	}

	@Test
	fun filterTableTest() {
		val result =
			globals.load(
				"""
				return filter(
					{1,2,3,4}, 
					function(v) 
						return v % 2 == 0 
					end
				)
				""".trimIndent(), "filterTableTest"
			).call()

		assert(coerceLuaToJava<Array<Int>>(result).contentEquals(arrayOf(2, 4)))
	}

	// TODO filter elements
	// TODO first

	@Test
	fun flattenTest() {
		val result = globals.load(
			"""
			return flatten({{1,2},{1,2}})
			""".trimIndent(),
			"flattenTest"
		).call()
		assert(coerceLuaToJava<Array<Int>>(result).contentEquals(arrayOf(1, 2, 1, 2)))
	}

	// TODO GET

	@Test
	fun mapTest() {
		val result =
			globals.load(
				"""
				return map(
					{1,2,3,4}, 
					function(v) 
						return v * 2
					end
				)
				""".trimIndent(), "mapTest"
			).call()

		assert(coerceLuaToJava<Array<Int>>(result).contentEquals(arrayOf(2, 4, 6, 8)))
	}

	// TODO map2flat

	fun mapNotNilTest() {
		// TODO mapNotNil
		val result =
			globals.load(
				"""
				return mapNotNil(
					{1,2,3,4}, 
					function(v) 
						if ( v % 2 == 0 ) then
							return nil
						end
						return v
					end
				)
				""".trimIndent(), "mapNotNilTest"
			).call()

		assert(coerceLuaToJava<Array<Int>>(result).contentEquals(arrayOf(1, 3)))
	}

	// TODO NodeVisitor

	@Test
	fun novelTest() {
		val title = "a"
		val imageURL = "b"
		val link = "c"
		val novel = Novel.Listing(
			title = title,
			imageURL = imageURL,
			link = link
		)
		val luaNovel = coerceLuaToJava<Novel.Listing>(
			globals.load(
				"""
					return Novel {
						title = "$title",
						imageURL = "$imageURL",
						link = "$link"
					}
				""".trimIndent(),
				"novelTest"
			).call()
		)
		assert(novel == luaNovel)
	}
	// TODO novel without table

	@Test
	fun chapterTest() {
		val title = "a"
		val link = "c"
		val release = "2023-01-14"
		val order = 0.0

		val chapter = Novel.Chapter(
			release = release,
			title = title,
			link = link,
			order = order
		)

		val luaChapter = coerceLuaToJava<Novel.Chapter>(
			globals.load(
				"""
					return NovelChapter {
						title = "$title",
						link = "$link",
						release = "$release",
						order = $order
					}
				""".trimIndent(),
				"chapterTest"
			).call()
		)
		assert(chapter == luaChapter)
	}
	// TODO chapter without table

	// TODO investigate reproducibility errors
	fun infoTest() {
		val title = "a"
		val alternativeTitles = arrayOf("b", "c")
		val imageURL = "link"
		val language = "eng"
		val description = ""
		val status = Novel.Status.COMPLETED
		val tags = arrayOf("d", "e", "f")
		val genres = arrayOf("g", "h", "i")
		val authors = arrayOf("j", "k", "l")
		val artists = arrayOf("m", "n", "o")
		val chapters = arrayOf<Novel.Chapter>()

		val info = Novel.Info(
			title = title,
			alternativeTitles = alternativeTitles,
			imageURL = imageURL,
			language = language,
			description = description,
			status = status,
			tags = tags,
			genres = genres,
			authors = authors,
			artists = artists,
			chapters = chapters
		)
		val luaInfo = coerceLuaToJava<Novel.Info>(
			globals.load(
				"""
					return NovelInfo {
						title = "$title",
						alternativeTitles = {"b","c"},
						imageURL = "$imageURL",
						language = "$language",
						description = "$description",
						status = NovelStatus.COMPLETED,
						tags = {"d","e","f"},
						genres = {"g","h","i"},
						authors = {"j","k","l"},
						artists = {"m","n","o"},
						chapters = {}
					}
				""".trimIndent(),
				"infoTest"
			).call()
		)
		println(info)
		println(luaInfo)
		assert(info == luaInfo)
	}
	// TODO info without table

	@Test
	fun novelStatusTest() {
		Novel.Status.values().forEach { type ->
			val result = globals.load("return NovelStatus.${type.name}", "novelStatusTest").call()
			assert(coerceLuaToJava<Novel.Status>(result) == type)
		}
	}

	// TODO pageOfElem
	// TODO pipeline

	// TODO POST
	// TODO wrap
}