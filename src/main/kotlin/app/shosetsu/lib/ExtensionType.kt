package app.shosetsu.lib

/**
 * Type of extension
 */
enum class ExtensionType {
	/**
	 * Represents a lua extension.
	 *
	 * File type is ".lua".
	 */
	LuaScript,
}
